package com.car_rental.minivan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MinivanApplication {

	public static void main(String[] args) {
		SpringApplication.run(MinivanApplication.class, args);
	}

}
