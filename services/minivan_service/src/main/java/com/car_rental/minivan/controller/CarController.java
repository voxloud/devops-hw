package com.car_rental.minivan.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cars")
public class CarController {

	@GetMapping
	public String list() {
		return "{\"totalPages\":2,\"empty\":false,\"totalElements\":3,\"size\":2,\"content\":[{\"name\":\"minivan_1\",\"color\":\"black\"},{\"name\":\"minivan_2\",\"color\":\"blue\"}]}";
	}

}