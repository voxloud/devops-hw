package com.car_rental.sedan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SedanApplication {

	public static void main(String[] args) {
		SpringApplication.run(SedanApplication.class, args);
	}

}
